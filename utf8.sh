#!/bin/bash
# 
# Script that converts MySQL collation from utf8mb4 to utf8 
# using a local MySQL server.
#
# Required:
# MySQL Server 5.5+
# A database user named 'convert' with create and drop privileges
# 
# Installation:
# Git clone
# chmod +x utf8.sh
#
# Usage: 
# utf8.sh filename.sql
# creates a compressed copy of a the converted sql dump
# 
# Based on Ben Lobaugh's awesome script
# http://ben.lobaugh.net/blog/201740/script-to-convert-mysql-collation-from-utf8mb4-to-utf8
#
# Daryl Lundy
# holla@databasedaryl.com
#
###############################
filename=$1
pass=convert
#
# check if file is valid sql dump
# function checkfile()
# {
# }

function convertdb()
{
   	echo Processing $filename...
   	# create temp database
    database=$(echo $filename | cut -f 1 -d.)
    db=$(echo $database'_utf8')
    mysql -p$pass -u convert -e "DROP DATABASE IF EXISTS $db";
	mysql -p$pass -u convert -e "CREATE DATABASE $db";
    # import sql file
    mysql -p$pass -u convert $db < $filename
    echo "converting tables..."
    # convert collation from utf8mb4 to utf8
	(
	    echo 'ALTER DATABASE `'"$db"'` CHARACTER SET utf8 COLLATE utf8_general_ci;'
	    mysql -p$pass -u convert "$db" -e "SHOW TABLES" --batch --skip-column-names \
	    | xargs -I{} echo 'ALTER TABLE `'{}'` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
	) \
	| mysql -p$pass -u convert "$db"
	# export compressed sql file
	echo "exporting new sql file"
	mysqldump -u convert -p$pass $db | gzip > $db.sql.gz
	# tidying things up
	mysql -p$pass -u convert -e "DROP DATABASE $db";
	echo "Done!"
	echo $database.sql has been exported as $db.sql.gz
}

if [ -f $filename ];
then
    #checkfile $filename not implimented
    convertdb;
else
	echo "Input file required"
	echo "Usage: ./utf8.sh filename.sql"
fi